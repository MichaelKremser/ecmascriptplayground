// Chapter 5, page 101
// Names changed for better understanding
function MyObject(name) {
    this.name = name;
}

MyObject.prototype.myName = function() {
    return this.name;
}

var book = new MyObject("YDNJS this & object prototypes");
console.log(`A book with name ${book.name}`);