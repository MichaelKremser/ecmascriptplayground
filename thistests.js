function identify()
{
    return this.name.toUpperCase();
}

var me = {
    name: "Michael"
}

console.time();
console.log(identify.call(me));
console.info("fyi");
console.timeEnd();